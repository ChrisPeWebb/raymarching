﻿using UnityEngine;
using System.Collections;

public class DeltaSettings : MonoBehaviour
{
    public Vector3 Offset = new Vector3(2.0f, 1.6f, 0.065f);
    public float YOffset = 0.74f;
    public float Scale = 1.8f;

    void Update ()
    {
        Shader.SetGlobalVector("_DeltaOffset", Offset);
        Shader.SetGlobalFloat("_DeltaYOffset", YOffset);
        Shader.SetGlobalFloat("_DeltaScale", Scale);
    }
}
