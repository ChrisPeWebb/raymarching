﻿#ifndef MAP_JULIA
#define MAP_JULIA

#include "RaymarcherCommon.cginc"
#include "Utils/DistanceFunctions.cginc"
#include "Utils/Math.cginc"


inline RaymarchOutput map(float3 position)
{
	// Rotate over time
	position = rotate(position, float3(_Time.y/300.0, _Time.y / 700.0, _Time.y / 500.0));

	int iterations = 4;
	float threshold = 16.0;
	float4 c = half4(0.41, 0.5, 0.2, 0.40);

	// Animate over time
	c.x = sin(_Time.y / 4.0) * 0.5 + 0.5;
	c.y = cos(_Time.y / 3.0) * 0.5 + 0.5;
	c.z = sin(_Time.y / 5.0) * 0.5 + 0.5;
	c.w = cos(_Time.y / 6.0) * 0.5 + 0.5;

	RaymarchOutput output;

	float4 p = float4(position, 0.0);
	float4 dp = float4(1.0, 0.0, 0.0, 0.0);

	for (int i = 0; i < iterations; ++i)
	{
		dp = float4( p.x * dp.x - dot(p.yzw, dp.yzw), 
			p.x * dp.yzw + dp.x * p.yzw + cross(p.yzw, dp.yzw)) * 2.0;

		p = float4(p.x * p.x - dot(p.yzw, p.yzw), float3(2.0 * p.x * p.yzw)) + c;

		float p2 = dot(p, p);

		if (p2 > threshold)
		{
			break;
		}
	}

	float r = length(p);
	output.distance = 0.5 * r * log(r) / length(dp);

	output.material = 0;

	return output;
}

#endif //!MAP_JULIA