﻿Shader "Hidden/RayMarcher" 
{
	Properties 
	{
		_MainTex( "Base (RGB)", 2D ) = "white" {}
	}

	Subshader
	{
		ZTest Always 
		Cull Off 
		ZWrite Off
		//Blend SrcAlpha OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#include "Map_DeltaCurve.cginc"
			//#include "Map_Test.cginc"
			//#include "Map_Julia.cginc"
			#include "SimpleRaymarcher.cginc"
			#pragma vertex vert_raymarcher
			#pragma fragment frag_raymarcher
			#pragma fragmentoption ARB_precision_hint_fastest 
			#pragma target 3.0
			ENDCG
		}
	}

	FallBack off
}
