﻿#ifndef DISTANCE_FUNCTIONS
#define DISTANCE_FUNCTIONS

//
// Primitives
//

float signedSphere(float3 p, float s)
{
	return length(p) - s;
}

float signedBox(float3 p, float3 b)
{
	float3 d = abs(p) - b;
	return min(max(d.x, max(d.y, d.z)), 0.0) + length(max(d, 0.0));
}


float signedTorus(float3 p, float2 t)
{
	float2 q = float2(length(p.xz) - t.x, p.y);
	return length(q) - t.y;
}


//
// Operations
//

float operationUnion(float d1, float d2)
{
	return min(d1, d2);
}

#endif //!DISTANCE_FUNCTIONS