﻿#ifndef Math
#define Math

// 360 / (PI * 2)
#define DEGREE_TO_RADIAN 57.2957795131


inline float degreeToRadian(float value)
{
	return value * DEGREE_TO_RADIAN;
}


inline float3 rotate(float3 pos, float3 rotation)
{
	float3 position = pos;

	float angleX = rotation.x * DEGREE_TO_RADIAN;
	float c = cos(angleX);
	float s = sin(angleX);
	float4x4 rotateXMatrix = float4x4(
		1, 0, 0, 0,
		0, c, -s, 0,
		0, s, c, 0,
		0, 0, 0, 1);

	float angleY = rotation.y * DEGREE_TO_RADIAN;
	c = cos(angleY);
	s = sin(angleY);
	float4x4 rotateYMatrix = float4x4(
		c, 0, s, 0,
		0, 1, 0, 0,
		-s, 0, c, 0,
		0, 0, 0, 1);

	float angleZ = rotation.z * DEGREE_TO_RADIAN;
	c = cos(angleZ);
	s = sin(angleZ);
	float4x4 rotateZMatrix = float4x4(
		c, -s, 0, 0,
		s, c, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1);

	position = mul(rotateXMatrix, position);
	position = mul(rotateYMatrix, position);
	position = mul(rotateZMatrix, position);

	return position;
}


float2 rand(float2 pos)
{
	return frac((pow(pos + 2.0, pos.yx + 1.0) * 22222.0));
}


float2 rand2(float2 pos)
{
	return rand(rand(pos));
}

#endif //!Math