﻿#ifndef RAYMARCHER_COMMON
#define RAYMARCHER_COMMON

#include "UnityCG.cginc"

uniform sampler2D _MainTex;
uniform half4 _MainTex_TexelSize;

uniform half _CameraFOV;
uniform half _CameraAspect;
uniform half3 _CameraPosition;
uniform half3 _CameraForward;
uniform half3 _CameraRight;
uniform half3 _CameraUp;


struct v2f_Raymarcher
{
	float4 pos : SV_POSITION;
	float4 vertex : TEXCOORD0;
	float2 uv : TEXCOORD1;
};


v2f_Raymarcher vert_raymarcher(appdata_img v)
{
	v2f_Raymarcher o;
	o.vertex = v.vertex;
	o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
	o.uv = v.texcoord.xy;

#if UNITY_UV_STARTS_AT_TOP
	if (_MainTex_TexelSize.y < 0)
		o.uv.y = 1 - o.uv.y;
#endif	

	o.uv = (o.uv - 0.5) * _CameraFOV;
	o.uv.x *= _CameraAspect;

	return o;
}


v2f_Raymarcher vert_raymarcher_deferred(appdata_img v)
{
	v2f_Raymarcher o;
	o.vertex = v.vertex;
	o.pos = v.vertex;
	o.uv = v.vertex.xy;

#if UNITY_UV_STARTS_AT_TOP
	//if (_MainTex_TexelSize.y < 0)
	//{
		o.pos.y = - o.pos.y;
		//o.uv.y = 1 - o.uv.y;
	//}
#endif	
	return o;
}


// The output of a raymarch
struct RaymarchOutput
{
	float distance; // Distance to surface
	int material;   // Material of closest surface
};


RaymarchOutput CombineOutputs(RaymarchOutput output0, RaymarchOutput output1)
{
	RaymarchOutput result;
	result.material = (output0.distance < output1.distance) ? output0.material : output1.material;
	result.distance = min(output0.distance, output1.distance); // boolean union
	return result;
}

#endif //!RAYMARCHER_COMMON