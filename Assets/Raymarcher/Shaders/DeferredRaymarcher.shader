﻿Shader "Hidden/Deferred RayMarcher" 
{
	Properties 
	{
	}

	Subshader
	{
		Tags{ "RenderType" = "Opaque" }
		Cull Off 

		Pass
		{
			Tags{ "LightMode" = "Deferred" }

			Stencil
			{
				Comp Always
				Pass Replace
				Ref 128
			}

			CGPROGRAM
			//#include "Map_DeltaCurve.cginc"
			//#include "Map_Test.cginc"
			#include "Map_Julia.cginc"
			#include "DeferredRaymarcher.cginc"
			#pragma vertex vert_raymarcher_deferred
			#pragma fragment frag_raymarcher
			#pragma fragmentoption ARB_precision_hint_fastest 
			#pragma target 4.0
			ENDCG
		}
	}

	FallBack off
}
