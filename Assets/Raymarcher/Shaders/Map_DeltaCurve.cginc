﻿#ifndef MAP_DELTACURVE
#define MAP_DELTACURVE

#include "RaymarcherCommon.cginc"
#include "Utils/DistanceFunctions.cginc"

uniform fixed3 _DeltaOffset;
uniform float _DeltaYOffset;
uniform float _DeltaScale;


inline RaymarchOutput map(float3 position)
{
	int iterations = 64; 
	float yOffset = 0.74; 
	float scale = 1.8; 
	float3 offset = float3(2.0, 1.6, 0.065);

	offset.xyz = _DeltaOffset.xyz;
	yOffset = _DeltaYOffset;
	scale = _DeltaScale;
	//offset.x = sin(_Time.y / 4.0) * 0.5 + 0.5;
	//offset.y = cos(_Time.y / 3.0) * 0.5 + 0.5;
	//offset.z = sin(_Time.y / 5.0) * 0.5 + 0.5;

	RaymarchOutput output;

	float r;
	int i = 0;
	while (i < iterations && dot(position, position) < 10000.0)
	{
		// Fold
		position.xy = abs(position.xy);

		if (position.y > position.x)
		{
			position.xy = position.yx;
		}

		position.y = yOffset - abs(position.y - yOffset);

		float ov = 1.0 / 3.0;

		// -+
		position.x += ov;
		if (position.z > position.x)
		{
			position.xz = position.zx;
		}
		position.x -= ov;

		// +-
		position.x -= ov;
		if (position.z > position.x)
		{
			position.xz = position.zx;
		}
		position.x += ov;

		position = scale * (position - offset) + offset;

		// TODO: Needs correct rotation (maybe)
		//position = rot *position;

		// TODO: Do orbit trap here

		r = dot(position, position);
		i++;
	}

	output.distance = abs(length(position) - length(offset)) * pow(scale, float(-i));

	output.material = 0;

	return output;
}

#endif //!MAP_DELTACURVE