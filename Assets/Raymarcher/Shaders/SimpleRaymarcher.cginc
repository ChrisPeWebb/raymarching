﻿#ifndef SIMPLE_RAYMARCHER
#define SIMPLE_RAYMARCHER

#include "RaymarcherCommon.cginc"
#include "Materials.cginc"
#include "Lighting.cginc"
#include "Utils/Math.cginc"


float3 CalculateNormal(in float3 position)
{
	float3 epsilon = float3(0.001, 0.0, 0.0);
	float3 normal = float3(
		map(position + epsilon.xyy).distance - map(position - epsilon.xyy).distance,
		map(position + epsilon.yxy).distance - map(position - epsilon.yxy).distance,
		map(position + epsilon.yyx).distance - map(position - epsilon.yyx).distance);
	return normalize(normal);
}


float CalculateAO(in float3 position, in float3 normal)
{
	int iterations = 4;
	float occlusion = 0.0;
	float weight = 1.0;

	for (int i = 0; i < iterations; ++i)
	{
		float hr = 0.01 + 0.12 * float(i) / 4.0;
		float3 aoSamplePosition = normal * hr + position;
		float aoSampleDistance = map(aoSamplePosition).distance;
		occlusion += -(aoSampleDistance - hr)*weight;
		weight *= 0.95;
	}
	return saturate(1.0 - 3.0 * occlusion);
}



float CalculateSoftShadow(in float3 rayOrigin, in float3 rayDirection, in float mint, in float tmax)
{
	float res = 1.0;
	float t = mint;
	for (int i = 0; i<16; i++)
	{
		float h = map(rayOrigin + rayDirection*t).distance;
		res = min(res, 8.0*h / t);
		t += clamp(h, 0.02, 0.10);

		if (h<0.001 || t>tmax)
		{
			break;
		}
	}
	return clamp(res, 0.0, 1.0);

}


half3 CalculateDirectLighting(in float3 rayPosition, in float3 rayDirection, in float3 normal)
{
	float3 lightDirection = _WorldSpaceLightPos0.xyz;
	half3 lightColor = _LightColor0.rgb;
	half3 ambientlightColor = unity_AmbientSky.rgb;

	float3 reflection = reflect(rayDirection, normal);

	float occlusion = CalculateAO(rayPosition, normal);
	float ambientLight = saturate(0.5 + 0.5 * normal.y) * occlusion;
	float diffuseLight = saturate(dot(lightDirection, normal)) * occlusion;
	float skyReflection = smoothstep(-0.1, 0.1, reflection.y);
	float fresnelLight = pow(saturate(1.0 + dot(normal, rayDirection)), 2.0);
	float specularLight = pow(saturate(dot(reflection, lightDirection)), 16.0);

	diffuseLight *= CalculateSoftShadow(rayPosition, lightDirection, 0.02, 2.5);
	skyReflection *= CalculateSoftShadow(rayPosition, reflection, 0.02, 2.5); // faked reflection ;)

	half3 light = half3(0.0, 0.0, 0.0);
	light += diffuseLight * lightColor;
	light += specularLight * lightColor * diffuseLight;
	light += ambientLight * ambientlightColor * occlusion;
	light += 0.30 * skyReflection * ambientlightColor * occlusion;
	light += fresnelLight * occlusion;

	return light;
}


#define TAU 6.2831853

float2 uniformDisc(float2 coord)
{
	float2 r = rand2(coord);
	return sqrt(r.y) * float2(cos(r.x * TAU), sin(r.x * TAU));
}


#define APERTURE 0.009
#define FOCAL_PLANE 0.4

half4 frag_raymarcher(v2f_Raymarcher IN) : SV_Target
{
	// TODO: Based on distance from center of disc, aberrate colors to get a nice CA effect on dof


	// Depth of field
	//float3 upOrtho = normalize(_CameraUp - dot(_CameraForward,_CameraUp)*_CameraForward);
	//float3 rightOrtho = normalize(cross(_CameraForward, upOrtho));

	//int subframe = int(_Time.y * 10.0);
	//subframe = 0;
	//float2 radius = APERTURE * uniformDisc(IN.vertex.xy * (float(subframe) + 1.0)); // viewcord
	//float2 disc = uniformDisc(IN.uv * float(1.0 + subframe)); // subsample jitter
	////float2 jitteredCoord = coord + AntiAliasScale*PixelScale*FOV*disc;
	//float2 jitteredCoord = IN.uv + disc *_CameraFOV;
	//float3 lensOffset = radius.x * _CameraRight + radius.y * _CameraUp;

	////float3 rayDirection = (_CameraForward + jitteredCoord.x*_CameraRight + jitteredCoord.y*_CameraUp) * FOCAL_PLANE - (lensOffset);
	////float3 rayDirection = jitteredCoord.y + _CameraUp * IN.uv.y + _CameraRight * IN.uv.x + _CameraForward + jitteredCoord.x;
	//float3 rayDirection = jitteredCoord.y + _CameraUp * IN.uv.y + _CameraRight * IN.uv.x + _CameraForward + jitteredCoord.x;
	//rayDirection = rayDirection * FOCAL_PLANE - (lensOffset);

	//rayDirection = normalize(rayDirection);

	float3 rayPosition = _CameraPosition;
	float3 rayDirection = _CameraUp * IN.uv.y + _CameraRight * IN.uv.x + _CameraForward;


	int iterations = 512;
	int breakoutDistance = 1000;
	float hitDistance = 0.0001;

	half4 outputColor = half4(0.0, 0.0, 0.0, 0.0);
	int i = 0;

	for (; i < iterations; ++i)
	{
		// Defined in a Map include (this represents the scene to render)
		RaymarchOutput output = map(rayPosition); 

		if (output.distance < hitDistance)
		{
			half3 albedo = GetAlbedoForMaterial(output.material);
			float3 normal = CalculateNormal(rayPosition);

			half3 directLight = CalculateDirectLighting(rayPosition, rayDirection, normal);

			outputColor.rgb = albedo * directLight;
			outputColor.a = 1.0;

			break;
		}

		// March ray forward by maximum distance
		rayPosition += rayDirection * output.distance; 

		if (output.distance > breakoutDistance)
		{
			break;
		}
	}

	outputColor.rgb = 0;

	// Edge Glow
	half3 glowColor = half3(1.0, 0.2, 0.0);
	glowColor = lerp(half3(1.0, 0.8, 0.0), half3(1.0, 0.2, 0.0), IN.uv.y);

	float edgeGlow = float(i) / iterations;
	edgeGlow = pow(edgeGlow, 0.75);
	//outputColor.rgb += half3(1.0, 0.2, 0.0) * edgeGlow;
	//outputColor.rgb += half3(2.0, 2.0, 2.0) * edgeGlow;
	outputColor.rgb += glowColor * edgeGlow;

	return outputColor;
}

#endif //!SIMPLE_RAYMARCHER