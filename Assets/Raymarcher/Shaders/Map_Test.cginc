﻿#ifndef MAP_TEST
#define MAP_TEST

#include "RaymarcherCommon.cginc"
#include "Utils/DistanceFunctions.cginc"


inline RaymarchOutput map(float3 position)
{
	RaymarchOutput output;

	RaymarchOutput sphere;
	sphere.distance = signedSphere(position, 1.0);
	sphere.material = 1;

	RaymarchOutput ground;
	ground.distance = signedBox(position + half3(0.0, 1.0, 0.0), float3(5.0, 0.1, 5.0));
	ground.material = 0;

	output = CombineOutputs(sphere, ground);

	RaymarchOutput torus;
	torus.distance = signedTorus(position + half3(1.5, 0.5, 2.5), float2(1.0, 0.25));
	torus.material = 2;

	output = CombineOutputs(output, torus);

	return output;
}

#endif //!MAP_TEST