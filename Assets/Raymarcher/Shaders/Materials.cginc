﻿#ifndef MATERIALS
#define MATERIALS

half4 GetAlbedoForMaterial(int material)
{
	switch (material)
	{
	case 0:
		return half4(0.5, 0.5, 0.5, 1.0);

	case 1:
		return half4(1.0, 0.5, 0.0, 1.0);

	case 2:
		return half4(0.0, 0.0, 1.0, 1.0);

	default:
		return half4(0.0, 0.0, 0.0, 0.0);
	}
}


half4 GetSmoothnessForMaterial(int material)
{
	switch (material)
	{
	case 0:
		return half4(1.0, 1.0, 1.0, 0.5);

	case 1:
		return half4(1.0, 1.0, 1.0, 0.95);

	case 2:
		return half4(1.0, 1.0, 1.0, 0.5);

	default:
		return half4(1.0, 1.0, 1.0, 0.5);
	}
}

#endif //!MATERIALS