﻿#ifndef DEFERRED_RAYMARCHER
#define DEFERRED_RAYMARCHER

#pragma target 4.0

#define UNITY_SHOULD_SAMPLE_SH 1
#define UNITY_SAMPLE_FULL_SH_PER_PIXEL 1

#include "RaymarcherCommon.cginc"
#include "Materials.cginc"

#include "UnityCG.cginc"
#include "UnityGlobalIllumination.cginc"
#include "Lighting.cginc"


#define HIT_DISTANCE 0.00001
#define ITERATIONS 512


float3 CalculateNormal(in float3 position)
{
	float3 epsilon = float3(0.001, 0.0, 0.0);
	float3 normal = float3(
		map(position + epsilon.xyy).distance - map(position - epsilon.xyy).distance,
		map(position + epsilon.yxy).distance - map(position - epsilon.yxy).distance,
		map(position + epsilon.yyx).distance - map(position - epsilon.yyx).distance);
	return normalize(normal);
}


float CalculateAO(in float3 position, in float3 normal)
{
	int iterations = 3;
	float occlusion = 0.0;
	float weight = 1.0;

	for (int i = 0; i < iterations; ++i)
	{
		float hr = 0.01 + 0.12 * float(i) / 4.0;
		float3 aoSamplePosition = normal * hr + position;
		float aoSampleDistance = map(aoSamplePosition).distance;
		occlusion += -(aoSampleDistance - hr)*weight;
		weight *= 0.95;
	}
	return saturate(1.0 - 3.0 * occlusion);
}


float CalculateDepth(float4 clipSpacePosition)
{
#if defined (UNITY_UV_STARTS_AT_TOP)
	return clipSpacePosition.z / clipSpacePosition.w;
#else
	return ((clipSpacePosition.z / clipSpacePosition.w) + 1.) * .5;
#endif
}


inline RaymarchOutput Intersect(in float3 rayOrigin, in float3 rayDirection, out float depth)
{
	depth = _ProjectionParams.y;
	float3 rayPosition = rayOrigin;
	RaymarchOutput raymarchOutput;

	for (int i = 0; i < ITERATIONS; ++i)
	{
		raymarchOutput = map(rayPosition);

		if (raymarchOutput.distance < HIT_DISTANCE)
		{
			break;
		}

		// March ray forward by maximum distance
		rayPosition += rayDirection * raymarchOutput.distance;

		depth += raymarchOutput.distance;

		// have we traveled further than the far clip plane?
		if (depth >= (_ProjectionParams.z - _ProjectionParams.y))
		{
			break;
		}
	}

	if (i == ITERATIONS)
	{
		depth = 1.0;
	}

	return raymarchOutput;
}


struct GBuffer
{
	half4 diffuse : SV_Target0;
	half4 specularSmoothness : SV_Target1;
	half4 normal : SV_Target2;
	half4 emission : SV_Target3;

	float depth : SV_Depth;
};


GBuffer frag_raymarcher(v2f_Raymarcher i)
{
	GBuffer output;

	//float3 rayPosition = _CameraPosition;
	//float3 rayDirection = _CameraUp * i.uv.y + _CameraRight * i.uv.x + _CameraForward;

	float3 rayPosition = _WorldSpaceCameraPos;
	float2 coordinates = i.uv;
	coordinates.x *= _ScreenParams.x / _ScreenParams.y;
	float3 rayDirection = normalize(float3(UNITY_MATRIX_V[0].xyz * coordinates.x +
		UNITY_MATRIX_V[1].xyz * coordinates.y - UNITY_MATRIX_V[2].xyz * abs(UNITY_MATRIX_P[1][1])));

	float depth = 0.0;
	RaymarchOutput raymarchOutput = Intersect(rayPosition, rayDirection, depth);

	if (raymarchOutput.distance < HIT_DISTANCE)
	{
		rayPosition = rayPosition + rayDirection * depth;

		half3 albedo = GetAlbedoForMaterial(raymarchOutput.material);
		float3 normal = CalculateNormal(rayPosition);
		float occlusion = CalculateAO(rayPosition, normal);

		output.depth = CalculateDepth(mul(UNITY_MATRIX_VP, float4(rayPosition, 1.0)));
		output.diffuse.rgb = albedo;
		output.diffuse.a = occlusion;
		output.normal = float4(normal * 0.5 + 0.5, 1.0);

		// TODO: get correct spec smoothness
		output.specularSmoothness = GetSmoothnessForMaterial(raymarchOutput.material);
		output.specularSmoothness.rgb *= albedo;

		half3 ambientLight = ShadeSHPerPixel(normal, float3(0, 0, 0), rayPosition);
		ambientLight *= occlusion;
		output.emission = float4(ambientLight, 0.0);
	}
	else
	{
		discard;
	}

	return output;
}

#endif //!DEFERRED_RAYMARCHER