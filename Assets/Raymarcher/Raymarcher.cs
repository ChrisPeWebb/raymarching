﻿using UnityEngine;

namespace Raymarcher
{
    public struct CameraPropertyIDs
    {
        public int FOV;
        public int Aspect;
        public int Position;
        public int Forward;
        public int Right;
        public int Up;

        public void CachePropertyIDs()
        {
            FOV = Shader.PropertyToID("_CameraFOV");
            Aspect = Shader.PropertyToID("_CameraAspect");
            Position = Shader.PropertyToID("_CameraPosition");
            Forward = Shader.PropertyToID("_CameraForward");
            Right = Shader.PropertyToID("_CameraRight");
            Up = Shader.PropertyToID("_CameraUp");
        }

        public void ApplyToMaterial(Material a_material, Camera a_camera)
        {
            a_material.SetFloat(FOV, Mathf.Tan(a_camera.fieldOfView * Mathf.Deg2Rad * 0.5f) * 2.0f);
            a_material.SetFloat(Aspect, a_camera.aspect);
            a_material.SetVector(Position, a_camera.transform.position);
            a_material.SetVector(Forward, a_camera.transform.forward);
            a_material.SetVector(Right, a_camera.transform.right);
            a_material.SetVector(Up, a_camera.transform.up);
        }
    }

    [ExecuteInEditMode]
    [RequireComponent(typeof(Camera))]
    public class Raymarcher : MonoBehaviour
    {
        [SerializeField]
        private Shader m_shader;

        private Material m_material;
        private bool m_isSupported = true;
        private Camera m_camera;
        private CameraPropertyIDs m_propertyIDs;


        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            if (CheckResources() == false)
            {
                Graphics.Blit(source, destination);
                return;
            }

            m_propertyIDs.ApplyToMaterial(m_material, m_camera);

            Graphics.Blit(source, destination, m_material, 0);
        }


        private bool CheckResources()
        {
            CheckSupport();

            m_material = CheckShaderAndCreateMaterial(m_shader, m_material);

            if (!m_isSupported)
            {
                ReportAutoDisable();
            }

            return m_isSupported;
        }


        private Material CheckShaderAndCreateMaterial(Shader s, Material m2Create)
        {
            if (!s)
            {
                Debug.Log("Missing shader in " + ToString());
                enabled = false;
                return null;
            }

            if (s.isSupported && m2Create && m2Create.shader == s)
            {
                return m2Create;
            }

            if (!s.isSupported)
            {
                NotSupported();
                Debug.Log("The shader " + s.ToString() + " on effect " + ToString() + " is not supported on this platform!");
                return null;
            }
            else
            {
                m2Create = new Material(s);
                m2Create.hideFlags = HideFlags.DontSave;
                if (m2Create)
                    return m2Create;
                else return null;
            }
        }


        private void Start()
        {
            CheckResources();
        }


        private void OnEnable()
        {
            m_isSupported = true;
            m_propertyIDs.CachePropertyIDs();
            m_camera = GetComponent<Camera>();
        }


        private void OnDisable()
        {
            if (m_material)
            {
                DestroyImmediate(m_material);
            }
        }


        private bool CheckSupport()
        {
            m_isSupported = true;

            if (!SystemInfo.supportsImageEffects || !SystemInfo.supportsRenderTextures)
            {
                NotSupported();
                return false;
            }

            return true;
        }


        private void NotSupported()
        {
            enabled = false;
            m_isSupported = false;
            return;
        }


        private void ReportAutoDisable()
        {
            Debug.LogWarning("The image effect " + ToString() + " has been disabled as it's not supported on the current platform.");
        }

    }
}