﻿using UnityEngine;
using UnityEngine.Rendering;

namespace Raymarcher
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(Camera))]
    public class DeferredRaymarcher : MonoBehaviour
    {
        [SerializeField]
        private Shader m_shader;

        private Material m_material;
        private bool m_isSupported = true;
        private Camera m_camera;
        private CameraPropertyIDs m_propertyIDs;
        private CommandBuffer m_commandBuffer;
        private Mesh m_fullScreenQuad;


        void OnPreRender()
        {
            if (CheckResources() == false)
            {
                return;
            }

            m_propertyIDs.ApplyToMaterial(m_material, m_camera);

            CreateCommandBufferIfRequired();
        }


        private void CreateCommandBufferIfRequired()
        {
            if (m_commandBuffer != null)
            {
                return;
            }

            m_commandBuffer = new CommandBuffer();
            m_commandBuffer.name = "Deferred Ray Marcher";
            CreateFullScreenQuadIfRequired();
            m_commandBuffer.DrawMesh(m_fullScreenQuad, Matrix4x4.identity, m_material, 0, 0, null);
            m_camera.AddCommandBuffer(CameraEvent.AfterGBuffer, m_commandBuffer);
        }


        private void CreateFullScreenQuadIfRequired()
        {
            if(m_fullScreenQuad != null)
            {
                return;
            }

            Vector3[] vertices = new Vector3[4]
            {
                new Vector3( 1.0f, 1.0f, 0.0f),
                new Vector3(-1.0f, 1.0f, 0.0f),
                new Vector3(-1.0f,-1.0f, 0.0f),
                new Vector3( 1.0f,-1.0f, 0.0f),
            };

            int[] indices = new int[6] 
            {
                0, 1, 2,
                2, 3, 0
            };

            m_fullScreenQuad = new Mesh();
            m_fullScreenQuad.vertices = vertices;
            m_fullScreenQuad.triangles = indices;
        }


        private bool CheckResources()
        {
            CheckSupport();

            m_material = CheckShaderAndCreateMaterial(m_shader, m_material);

            if (!m_isSupported)
            {
                ReportAutoDisable();
            }

            return m_isSupported;
        }


        private Material CheckShaderAndCreateMaterial(Shader s, Material m2Create)
        {
            if (!s)
            {
                Debug.Log("Missing shader in " + ToString());
                enabled = false;
                return null;
            }

            if (s.isSupported && m2Create && m2Create.shader == s)
            {
                return m2Create;
            }

            if (!s.isSupported)
            {
                NotSupported();
                Debug.Log("The shader " + s.ToString() + " on effect " + ToString() + " is not supported on this platform!");
                return null;
            }
            else
            {
                m2Create = new Material(s);
                m2Create.hideFlags = HideFlags.DontSave;
                if (m2Create)
                {
                    return m2Create;
                }
                else
                {
                    return null;
                }
            }
        }


        private void Start()
        {
            CheckResources();
        }


        private void OnEnable()
        {
            m_isSupported = true;
            m_propertyIDs.CachePropertyIDs();
            m_camera = GetComponent<Camera>();
            m_camera.depthTextureMode = DepthTextureMode.Depth;
        }


        private void OnDisable()
        {
            if (m_material)
            {
                DestroyImmediate(m_material);
            }

            if(m_camera != null)
            {
                if(m_commandBuffer != null)
                {
                    m_camera.RemoveCommandBuffer(CameraEvent.AfterGBuffer, m_commandBuffer);
                }
            }

            m_commandBuffer = null;
        }


        private bool CheckSupport()
        {
            m_isSupported = true;

            if (!SystemInfo.supportsImageEffects || !SystemInfo.supportsRenderTextures)
            {
                NotSupported();
                return false;
            }

            return true;
        }


        private void NotSupported()
        {
            enabled = false;
            m_isSupported = false;
            return;
        }


        private void ReportAutoDisable()
        {
            Debug.LogWarning("The effect " + ToString() + " has been disabled as it's not supported on the current platform.");
        }

    }
}